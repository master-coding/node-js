let numbers = [10, 30, 20, 40];
console.log(numbers[2]);
numbers[2] = 25;
console.log(numbers);

console.log(typeof numbers);

numbers.push(50);
numbers.push(60);
console.log(numbers);

numbers.pop();
console.log(numbers);

console.log("The index of element 30 is : ", numbers.indexOf(30));
console.log("The index of non exist element is : ", numbers.indexOf(300));

let names = [3, "Lakshman", "Bharath"];
console.log(names);

console.log("length of an array = " + numbers.length);
numbers.reverse();
console.log("reversed array = ", numbers);

//sort - alphabetical order
let names1 = ["RAM", "Lakshman", "Bharath"];
console.log(names1.sort());

let numbers1 = [20, 10, 1, 123, 25];
console.log(numbers1.sort());
function compare(x, y) {
    if( x > y){
        return 1;
    }else{
        return -1;
    }
}
console.log(numbers1.sort(compare));

console.log(["we", "are", "learning", "js"].join(" "));

console.log([1,2,3,4,5].concat([6,7,8,9]));

console.log([10,20,30,40,50].slice(2));

console.log([10,20,30,40,50].slice(1, 3));

let z = [0,1,2,3,4];
delete z[2];
console.log(z);

for(let i=0; i < z.length; i++ ){
    console.log(z[i]);
}