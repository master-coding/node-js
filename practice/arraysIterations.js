let numbers = [ 10, 5, 20, 7, 30, 5, 40];

for(let i = 0; i < numbers.length; i++){
    console.log(numbers[i]);
}

console.log("----------------------");
function print(a){
    console.log(a);
}
numbers.forEach(print);
console.log("----------------------");

//giving argument as anonymous function
numbers.forEach(function (a) {
    console.log(a);
});

let squares = numbers.map(function(a){
   return a * a;
});

console.log(squares);


let singleDigits = numbers.filter(function (num) {
    if(num >= 10 || num <= -10){
        return false;
    }else {
        return true;
    }
});
console.log(singleDigits);

let sum = numbers.reduce(function(total, num){
  return total + num;
});

console.log(sum);