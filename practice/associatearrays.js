// associative arrays
let phoneNumbers = {};
phoneNumbers[900] = "RAM";
phoneNumbers[901] = "LAK";
phoneNumbers[902] = "BHARATH";
phoneNumbers[903] = "JOHN";

console.log(typeof phoneNumbers);

console.log(phoneNumbers);
console.log(phoneNumbers[900]);

delete phoneNumbers[902];
console.log(phoneNumbers);

// console.log(phoneNumbers.length);
