//type inference
var i = 10;
console.log("10 is of type " + typeof i);

var j = 10.323;
console.log("10.323 is of type " + typeof j);

var k = true;
console.log("true is of type " + typeof k);

var l = "JavaScript";
console.log("JavaScript is of type " + typeof l);

var m;
console.log("unintialized variable is of type " + typeof m);


var n = [1, 2, 3];
console.log("[1,2,3] is of type " + typeof n);

var o = {
  "name" : "swagat",
  "area" : "nagole"
};

console.log("{\"name\": \"swagat\"} is of type : ", typeof o);

var p = null;
console.log("null is of type : ", typeof p)

//variables var, let, const

var q = 2;
var q = 20;
console.log("redefined q is : " + q);

//prefer let over var
let q1 = 3;
// let q1 = 20;
console.log("redifned let q1 is : ", q1);

const r = 20;
// r = 40;
console.log("const r is : ", r);
