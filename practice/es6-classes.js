class Account {
    constructor(amount){
        this.balance = amount;
    }

    get getBalance() {
        return this.balance;
    }

    set setBalance(amount){
        this.balance = amount;
    }

    deposit(amount) {
        this.balance = this.balance + amount;
        Account.logTransaction()
    }

    withdraw(amount) {
        this.balance = this.balance - amount;
        Account.logTransaction()
    }

    static logTransaction() {
        Account.count++;
    }

}
Account.count = 0;

let ramAcc = new Account(1000);
ramAcc.setBalance = 500;
console.log(ramAcc.getBalance);

ramAcc.deposit(300);
ramAcc.withdraw(200);

let lakAccount = new Account(300);
lakAccount.deposit(200);

console.log(ramAcc.getBalance);

console.log(`The number of transactions are : ${Account.count}`);

class SavingAccount extends Account{
    deposit(amount){
        super.deposit(amount + 2);
    }
}

let bharathAcc = new SavingAccount(100);
bharathAcc.deposit(500);
console.log(bharathAcc.balance);

console.log(`Number of transactions = ${Account.count}`);