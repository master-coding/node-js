function max(a, b) {
    return a > b ? a : b;
}

console.log(max(10, 20));
console.log(max(10.5, 20.4));
console.log(max("RAM", "LAK"));

//passing function max as argument
test(max);

function test(m) {
    console.log( m(10, 20) );
}

//returning function from another function
function test1() {
    return max;
}
let m = test1();
console.log(m(45, 34));


let i = 20;

let s = function sum(a, b) {
    return a + b;
};

console.log(s(10, 20));

