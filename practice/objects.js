let animal = {
    "name": "dog",
    "weight": 12,
    "details": function () {
        return this.name + " : " + this.weight;
    }
};

console.log(animal.details());

for(let prop in animal){
    console.log(animal[prop]);
}

let account = {
  "balance": 0,
  "deposit": function (amount) {
      this.balance = this.balance + amount;
  },
  "withdraw": function (amount) {
      this.balance = this.balance - amount;
  }
};

account.deposit(1000);
account.withdraw(300);
console.log(account.balance);

function Account(initialBalance) {
    this.balance = initialBalance;
    this.deposit = function (amount) {
        this.balance += amount;
    };
    this.withdraw = function (amount) {
        this.balance -= amount;
    };
}

let ramAccount = new Account(1000);
ramAccount.deposit(400);
let lakAccount = new Account(500);
lakAccount.withdraw(100);

console.log(`The Ram Balance = ${ramAccount.balance}`);
console.log(`The Lak Balance = ${lakAccount.balance}`);