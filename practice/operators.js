// === ==
let i = 10;
let j = 10;

if (i == j) {
    console.log("10 == 10 are : equal");
} else {
    console.log("10 == 10 are : not equal");
}

if (i === j) {
    console.log("10 === 10 are : equal");
} else {
    console.log("10 === 10 are : not equal");
}


let i1 = 10;
let j1 = "10";
if (i1 == j1) {
    console.log("10 == \"10\" are : equal");
} else {
    console.log("10 == \"10\" are : not equal");
}

if (i1 === j1) {
    console.log("10 === \"10\" are : equal");
} else {
    console.log("10 === \"10\" are : not equal");
}

// !== vs !=
let i3 = 10;
let j3 = "10";
//checks only data
if (i3 != j3) {
    console.log("10 != \"10\" is : true");
} else {
    console.log("10 != \"10\" is : false");
}

//checks type and the data
if (i3 !== j3) {
    console.log("10 !== \"10\" is : true");
} else {
    console.log("10 !== \"10\" is : false");
}
